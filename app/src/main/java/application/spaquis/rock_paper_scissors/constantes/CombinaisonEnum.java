package application.spaquis.rock_paper_scissors.constantes;

import android.support.annotation.NonNull;

/**
 * Enumération des combinaisons possibles
 * <p>
 * Created by spaquis on 19/08/2017.
 */
public enum CombinaisonEnum {
    PAPER_ROCK(ActionEnum.PAPER, ActionEnum.ROCK, ActionEnum.PAPER),
    PAPER_SCISSORS(ActionEnum.PAPER, ActionEnum.SCISSORS, ActionEnum.SCISSORS),
    ROCK_SCISSORS(ActionEnum.ROCK, ActionEnum.SCISSORS, ActionEnum.ROCK);

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    /* Action 1 */
    private ActionEnum actionOne;

    /* Action 2 */
    private ActionEnum actionTwo;

    /* Gagnant */
    private ActionEnum gagnant;

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    /**
     * Constructeur
     *
     * @param actionOne : action1
     * @param actionTwo : action 2
     * @param gagnant   : gagnant
     */
    CombinaisonEnum(ActionEnum actionOne, ActionEnum actionTwo, ActionEnum gagnant) {
        this.actionOne = actionOne;
        this.actionTwo = actionTwo;
        this.gagnant = gagnant;
    }


    // -------------------------------------- Méthodes statiques publiques

    /**
     * Méthode permettant de récupérer la combinaison
     *
     * @param action1 : action 1
     * @param action2 : action 2
     * @return : la combinaison
     */
    public static CombinaisonEnum getCombinaison(@NonNull ActionEnum action1, @NonNull ActionEnum action2) {
        for (CombinaisonEnum combinaisonEnum : CombinaisonEnum.values()) {
            if (combinaisonEnum.getActionOne().equals(action1) && combinaisonEnum.getActionTwo().equals(action2)
                    || combinaisonEnum.getActionOne().equals(action2) && combinaisonEnum.getActionTwo().equals(action1)) {
                return combinaisonEnum;
            }
        }
        return null;
    }

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    // -------------------------------------- Accesseurs

    /**
     * Récupération action 1
     *
     * @return : action 1
     */
    public ActionEnum getActionOne() {
        return actionOne;
    }

    /**
     * Set action 1
     *
     * @param actionOne : action 1
     */
    public void setActionOne(ActionEnum actionOne) {
        this.actionOne = actionOne;
    }

    /**
     * Récupération action 2
     *
     * @return : action 2
     */
    public ActionEnum getActionTwo() {
        return actionTwo;
    }

    /**
     * Set action 2
     *
     * @param actionTwo : action 2
     */
    public void setActionTwo(ActionEnum actionTwo) {
        this.actionTwo = actionTwo;
    }

    /**
     * Récupération gagnant
     *
     * @return : gagnant
     */
    public ActionEnum getGagnant() {
        return gagnant;
    }

    /**
     * Set gagnant
     *
     * @param gagnant : gagnant
     */
    public void setGagnant(ActionEnum gagnant) {
        this.gagnant = gagnant;
    }
}
