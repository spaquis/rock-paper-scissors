package application.spaquis.rock_paper_scissors.constantes;

/**
 * Classe des énumérations des players
 * <p>
 * Created by spaquis on 19/08/2017.
 */
public enum PlayerEnum {

    COMPUTER_1("Computer 1"), COMPUTER_2("Computer 2"), PLAYER_1("Player 1");

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    /* Nom de du player */
    private String name;

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    /**
     * Constructeur
     *
     * @param name : nom
     */
    private PlayerEnum(String name) {
        this.name = name;
    }

    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    // -------------------------------------- Accesseurs

    /**
     * Récupération du nom
     *
     * @return : nom
     */
    public String getName() {
        return name;
    }

    /**
     * Set du nom
     *
     * @param name : nom
     */
    public void setName(String name) {
        this.name = name;
    }
}
