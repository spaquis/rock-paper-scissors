package application.spaquis.rock_paper_scissors.constantes;

/**
 * Classe des énumérations des résutlats possibles
 * <p>
 * Created by spaquis on 19/08/2017.
 */
public enum ResultEnum {

    EGALITE("Equality"), GAGNE("Win"), PERDU("Loose");

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    /* Nom de l'action */
    private String name;

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    /**
     * Constructeur
     *
     * @param name : nom
     */
    private ResultEnum(String name) {
        this.name = name;
    }

    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    // -------------------------------------- Accesseurs

    /**
     * Récupération du nom
     *
     * @return : nom
     */
    public String getName() {
        return name;
    }

    /**
     * Set du nom
     *
     * @param name : nom
     */
    public void setName(String name) {
        this.name = name;
    }
}
