package application.spaquis.rock_paper_scissors.constantes;

/**
 * ac
 * Classe des énumérations des modes possibles
 * <p>
 * Created by spaquis on 19/08/2017.
 */
public enum ModeEnum {

    COMPUTER_COMPUTER(1, "COMPUTER\n VS \nCOMPUTER"), PLAYER_COMPUTER(2, "PLAYER\n VS \nCOMPUTER");

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    /* Identifiant du mode */
    private Integer id;

    /* Nom de lu mode */
    private String name;

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    /**
     * Constructeur
     *
     * @param id   : identifiant
     * @param name : nom
     */
    private ModeEnum(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    // -------------------------------------- Méthodes statiques publiques

    /**
     * Méthode permettant de récupérer l'énum via le nom de l'action
     *
     * @param name : nom de l'action
     * @return
     */
    public static ModeEnum getFromName(String name) {
        for (ModeEnum actionEnum : ModeEnum.values()) {
            if (actionEnum.getName().equals(name)) {
                return actionEnum;
            }
        }
        return null;
    }

    /**
     * Méthode permettant de récupérer l'énum via l'identifiant de l'action
     *
     * @param identifiant : identifiant du mode
     * @return
     */
    public static ModeEnum getFromId(Integer identifiant) {
        for (ModeEnum actionEnum : ModeEnum.values()) {
            if (actionEnum.getId().equals(identifiant)) {
                return actionEnum;
            }
        }
        return null;
    }

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    // -------------------------------------- Accesseurs

    /**
     * Récupération de l'identifiant
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set de l'identifiant
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Récupération du nom
     *
     * @return : nom
     */
    public String getName() {
        return name;
    }

    /**
     * Set du nom
     *
     * @param name : nom
     */
    public void setName(String name) {
        this.name = name;
    }
}
