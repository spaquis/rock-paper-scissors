package application.spaquis.rock_paper_scissors.constantes;

/**
 * Classe des énumérations des actions possibles
 * <p>
 * Created by spaquis on 19/08/2017.
 */
public enum ActionEnum {

    ROCK(1, "Rock"), PAPER(2, "Paper"), SCISSORS(3, "Scissors");

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    /* Identifiant du mode */
    private Integer id;

    /* Nom de l'action */
    private String name;

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    /**
     * Constructeur
     *
     * @param name : nom
     */
    private ActionEnum(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    // -------------------------------------- Méthodes statiques publiques

    /**
     * Méthode permettant de récupérer l'énum via le nom de l'action
     *
     * @param name : nom de l'action
     * @return : l'action
     */
    public static ActionEnum getFromName(String name) {
        for (ActionEnum actionEnum : ActionEnum.values()) {
            if (actionEnum.getName().equals(name)) {
                return actionEnum;
            }
        }
        return null;
    }

    /**
     * Méthode permettant de récupérer l'énum via l'id de l'action
     *
     * @param id : id de l'action
     * @return : l'action
     */
    public static ActionEnum getFromId(Integer id) {
        for (ActionEnum actionEnum : ActionEnum.values()) {
            if (actionEnum.getId().equals(id)) {
                return actionEnum;
            }
        }
        return null;
    }

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    // -------------------------------------- Accesseurs

    /**
     * Récupération de l'identifiant

     * @return : l'identifiant
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set de l'identifiant
     * @param id : l'identifiant
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Récupération du nom
     *
     * @return : nom
     */
    public String getName() {
        return name;
    }

    /**
     * Set du nom
     *
     * @param name : nom
     */
    public void setName(String name) {
        this.name = name;
    }
}
