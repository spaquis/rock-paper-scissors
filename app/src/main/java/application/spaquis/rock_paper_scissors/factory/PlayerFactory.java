package application.spaquis.rock_paper_scissors.factory;

import android.util.Log;
import application.spaquis.rock_paper_scissors.exception.BusinessException;
import application.spaquis.rock_paper_scissors.model.PlayerModel;

/**
 * Singleton d'instancier et d'accéder à des Players
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public class PlayerFactory extends ParentFactory<PlayerModel> {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques privés

    /* Le tag de la classe */
    private static final String TAG = "PlayerFactory";

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    /* L'instance du Singleton */
    private static PlayerFactory instance;

    /* Le generator d'identifiant */
    private Integer idGenerator = 1;

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    /**
     * Contruteur
     */
    private PlayerFactory() {
    }

    // -------------------------------------- Méthodes statiques publiques

    /**
     * Méthode permettant de récupérer l'instance du Singleton
     *
     * @return : instance du Singleton
     */
    public static PlayerFactory getInstance() {
        if (instance == null) {
            instance = new PlayerFactory();
        }
        return instance;
    }


    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    /**
     * Méthode permettant d'dajouter un player
     *
     * @param name : le nom du player
     * @return : le player
     * @throws : Business exception
     */
    @Override
    public PlayerModel add(String name) throws BusinessException {
        Log.i(TAG, "Tentative d'ajout d'un player : " + name);
        // Vérification de l'existence
        if (!this.checkIfExist(name)) {
            PlayerModel newPlayer = new PlayerModel(idGenerator, name);
            Log.i(TAG, "Ajout du player : " + newPlayer.toString());
            this.addToMap(name, newPlayer);
            idGenerator++;
            return newPlayer;
        } else {
            String message = "Le player : " + name + " existe déjà.";
            Log.w(TAG, message);
            return this.getObj(name);
        }
    }

    // -------------------------------------- Accesseurs
}
