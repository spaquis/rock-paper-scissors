package application.spaquis.rock_paper_scissors.factory;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import application.spaquis.rock_paper_scissors.exception.BusinessException;

/**
 * Classe abstraite parente des factory
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public abstract class ParentFactory<T> {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques privés

    /* Le tag de la classe */
    private static final String TAG = "ParentFactory";

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    /* Map contenant la liste des data */
    private final Map<String, T> mapData = new HashMap<>();

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    // -------------------------------------- Méthodes protected

    /**
     * Méthode permettant de savoir si l'element existe dans la map
     *
     * @param key : la cle
     * @return : true si exist, false sinon
     */
    protected boolean checkIfExist(String key) {
        return this.mapData.containsKey(key);
    }

    /**
     * Ajout de l'élement à la liste
     *
     * @param key : la clé
     * @param obj : l'obj
     */
    protected void addToMap(String key, T obj) {
        this.mapData.put(key, obj);
    }

    // -------------------------------------- Méthodes publiques

    /**
     * Méthode permettant d'ajouter un element à la liste
     *
     * @param key
     * @return
     * @throws BusinessException
     */
    public abstract T add(String key) throws BusinessException;

    /**
     * Récupération de la taille de la map
     */
    public int getTailleMap() {
        return mapData.size();
    }

    /**
     * Méthode permettant de récupérer l'objet via sa key
     *
     * @param key : la clé
     * @return : l'objet
     * @throws BusinessException : si l'objet n'existe pas dans la liste
     */
    public T getObj(String key) throws BusinessException {
        if (!this.checkIfExist(key)) {
            String message = "Il n'existe pas d'élèment : " + key + " dans la liste";
            Log.w(TAG, message);
            throw new BusinessException(message);
        }
        return mapData.get(key);
    }

    /**
     * Clear de la map
     */
    public void clearMap() {
        this.mapData.clear();
    }

    // -------------------------------------- Accesseurs

}
