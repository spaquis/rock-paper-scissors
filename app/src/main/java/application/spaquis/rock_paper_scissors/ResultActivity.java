package application.spaquis.rock_paper_scissors;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import application.spaquis.rock_paper_scissors.constantes.ActionEnum;

/**
 * Activty sur le résultat
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public class ResultActivity extends AppCompatActivity {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques privés

    /* Le tag de la classe */
    private static final String TAG = "ResultActivity";

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    /* le text du resultat */
    private TextView resultat_text;

    /* Le player 1 */
    private TextView player1Detail;

    /* Le player 2 */
    private TextView player2Detail;

    /* Le button de retour */
    private Button backButton;

    /* Listener sur le click d'un button de retour */
    private final View.OnClickListener btnBackClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(TAG, "Redirection vers l'activity Home");
            backToHome();
        }
    };

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    /**
     * Méthode permettant de revenir sur l'accueil
     */
    private void backToHome () {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    /**
     * Méthode permettant de constituer le détail d'une action
     * @param actionId : l'id de l'action
     * @param playerName : le nom du player
     * @return : le détail
     */
    private String constituerDetail(Integer actionId, String playerName) {
        StringBuilder detail = new StringBuilder();
        detail.append(playerName);
        detail.append(" played ");
        detail.append(ActionEnum.getFromId(actionId).getName());
        return detail.toString();
    }

    // -------------------------------------- Méthodes protected

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Création de l'activity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        // Récupération des composants
        resultat_text = (TextView) findViewById(R.id.resultat_text);
        player1Detail = (TextView) findViewById(R.id.player1_text);
        player2Detail = (TextView) findViewById(R.id.player2_text);
        backButton = (Button) findViewById(R.id.back_button);
        backButton.setOnClickListener(btnBackClicked);

        // Récupération des infos de l'écran précédent
        Intent intent = getIntent();
        String player1 = intent.getStringExtra(GameActivity.PLAYER_ONE_TOKEN);
        Integer action1 = intent.getIntExtra(GameActivity.ACTION_ONE_TOKEN, 1);
        String player2 = intent.getStringExtra(GameActivity.PLAYER_TWO_TOKEN);
        Integer action2 = intent.getIntExtra(GameActivity.ACTION_TWO_TOKEN, 1);
        String resultat = intent.getStringExtra(GameActivity.RESULT_TOKEN);

        Log.d(TAG, "Params récupéré, player1 : " + player1 + ", player2 : " + player2 + ", action1 : " +
                action1 + ", action2 : " + action2 + ", resultat : " + resultat);

        // Alimentation des champs
        player1Detail.setText(constituerDetail(action1, player1));
        player2Detail.setText(constituerDetail(action2, player2));
        resultat_text.setText(resultat);
    }

    // -------------------------------------- Méthodes publiques

    // -------------------------------------- Accesseurs
}
