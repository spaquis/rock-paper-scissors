package application.spaquis.rock_paper_scissors;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import application.spaquis.rock_paper_scissors.business.CalculMotor;
import application.spaquis.rock_paper_scissors.constantes.ActionEnum;
import application.spaquis.rock_paper_scissors.constantes.ModeEnum;
import application.spaquis.rock_paper_scissors.exception.BusinessException;
import application.spaquis.rock_paper_scissors.model.ResultModel;

/**
 * Activity de la partie
 * <p>
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public class GameActivity extends AppCompatActivity {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques privés

    /* Le tag de la classe */
    private static final String TAG = "GameActivity";

    // -------------------------------------- Attributs statiques publics

    /* Token representant le resultat */
    public static final String RESULT_TOKEN = "RESULT";

    /* Token representant le player 1 */
    public static final String PLAYER_ONE_TOKEN = "PLAYER_ONE";

    /* Token representant l'action 1 */
    public static final String ACTION_ONE_TOKEN = "ACTION_ONE";

    /* Token representant le player 2 */
    public static final String PLAYER_TWO_TOKEN = "PLAYER_TWO";

    /* Token representant l'action 2 */
    public static final String ACTION_TWO_TOKEN = "ACTION_TWO";

    // -------------------------------------- Attributs privés
    /* le text du mode */
    private TextView textMode;

    /* Le mode Courant */
    private ModeEnum currentMode;

    /* Le bouton actif */
    private Button buttonActif;

    /* Le bouton démarrer */
    private Button buttonStart;

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    /* le layout des actions */
    private LinearLayout layoutAction;

    /* Le layout du button démarrer */
    private LinearLayout layoutStart;

    /* Les différentes actions possibles */
    private Button[] btnAction;

    /* Listener sur le click d'une action */
    private final View.OnClickListener btnClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.i(TAG, "Click sur l'action " + v.getTag());
            Object tag = v.getTag();
            // On reset le style du précédent button actif
            if (buttonActif != null) {
                initDefaultStyleButton(buttonActif);
            }

            buttonActif = (Button) v;

            // On set le style du nouveau button actif
            initActifStyleButton(buttonActif);

            layoutStart.setVisibility(View.VISIBLE);
        }
    };

    /* Listener sur le click du button démarrer */
    private final View.OnClickListener startClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.i(TAG, "Click sur le button démarrer");
            ResultModel resultat = null;
            try {
                if (ModeEnum.COMPUTER_COMPUTER.equals(currentMode)) {
                    // Duel Computer / Computer
                    resultat = CalculMotor.getInstance().proceedComputerGame();
                } else {
                    // Duel Player / Computer
                    resultat = CalculMotor.getInstance().proceedPlayerGame((ActionEnum) buttonActif.getTag());
                }
            }
            catch (BusinessException e) {
                Toast.makeText(getApplicationContext(), "Une erreur est survenue", Toast.LENGTH_SHORT).show();
            }

            Log.i(TAG, "Partie terminé : " + resultat.toString());
            goToResultat(resultat);
        }
    };

    /**
     * Méthode permettant de setter le style par default
     * @param button
     */
    private void initDefaultStyleButton (Button button) {
        button.setBackgroundColor(getResources().getColor(R.color.colorGrey));
        button.setTextColor(getResources().getColor(R.color.colorDark));
    }

    /**
     * Méthode permettant de setter le style actif d'un button
     * @param button
     */
    private void initActifStyleButton (Button button) {
        button.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        button.setTextColor(getResources().getColor(R.color.colorWhite));
    }

    /**
     * Méthode permettant de créer un button action
     * @param action : l'action
     * @return : le button
     */
    private Button createButtonAction(ActionEnum action) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(30, 30, 30, 30);
        Button button = new Button(this);
        this.initDefaultStyleButton(button);
        button.setTag(action);
        button.setOnClickListener(btnClicked);
        button.setText(action.getName());
        button.setLayoutParams(params);
        return button;
    }

    /**
     * Méthode permettant de se rediriger vers la page de Résultat
     *
     * @param resultat : le resultat du match
     */
    private void goToResultat(ResultModel resultat) {
        Log.d(TAG, "Redirection vers l'activity Result");
        Intent intent = new Intent(this, ResultActivity.class);

        intent.putExtra(PLAYER_ONE_TOKEN, resultat.getPlayerOne().getName());
        intent.putExtra(ACTION_ONE_TOKEN, resultat.getActionPlayerOne().getId());
        intent.putExtra(PLAYER_TWO_TOKEN, resultat.getPlayerTwo().getName());
        intent.putExtra(ACTION_TWO_TOKEN, resultat.getActionPlayerTwo().getId());
        intent.putExtra(RESULT_TOKEN, resultat.getResutat());
        Log.d(TAG, "Paramètre " + intent.toString());
        startActivity(intent);
    }

    // -------------------------------------- Méthodes protected

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Création de l'activity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // Récupération des composants
        textMode = (TextView) findViewById(R.id.mode_textView);
        layoutAction = (LinearLayout) findViewById(R.id.actions_layout);
        layoutStart = (LinearLayout) findViewById(R.id.start_layout);
        buttonStart = (Button) findViewById(R.id. start_button);
        buttonStart.setOnClickListener(startClicked);

        // Récupération des infos de l'écran précédent
        Intent intent = getIntent();
        Integer modeId = intent.getIntExtra(HomeActivity.MODE_TOKEN, 1);
        Log.d(TAG, "Récupération du mode " + modeId);
        currentMode = ModeEnum.getFromId(modeId);
        textMode.setText(currentMode.getName());

        //TODO Ne créer les actions que dans le cas ou est en MODE PLAYER / COMPUTER

        if(ModeEnum.PLAYER_COMPUTER.equals(currentMode)) {
            // Affichage du button démarrer seulement quand l'utilisateur aura fait son choix
            layoutStart.setVisibility(View.INVISIBLE);
            Log.d(TAG, "Création des buttons actions");
            btnAction = new Button[ActionEnum.values().length];
            for (ActionEnum actionEnum : ActionEnum.values()) {
                Button button = this.createButtonAction(actionEnum);
                layoutAction.addView(button);
            }
        } else {
            layoutAction.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Démarrage de l'actitivty
     */
    @Override
    protected void onStart() {
        Log.d(TAG, "Démarrage de l'activity");
        super.onStart();
    }

    // -------------------------------------- Méthodes publiques

    // -------------------------------------- Accesseurs
}
