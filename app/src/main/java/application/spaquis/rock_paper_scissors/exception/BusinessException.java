package application.spaquis.rock_paper_scissors.exception;

/**
 * Exception fonctionnelle
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public class BusinessException extends Exception {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques privés

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    /**
     * Constructeur
     *
     * @param message : message
     */
    public BusinessException(String message) {
        super(message);
    }

    /**
     * Constructeur
     *
     * @param message : message
     * @param cause   : cause
     */
    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }


    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    // -------------------------------------- Accesseurs
}
