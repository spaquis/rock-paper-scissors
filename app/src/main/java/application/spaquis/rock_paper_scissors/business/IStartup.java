package application.spaquis.rock_paper_scissors.business;

import application.spaquis.rock_paper_scissors.exception.BusinessException;

/**
 * Interface du business de startup
 * <p>
 * Created by spaquis on 19/08/2017.
 */
public interface IStartup {

    // -------------------------------------- Méthodes publiques

    /**
     * Initialisation de l'app
     *
     * @throws BusinessException
     */
    public void initApp() throws BusinessException;
}
