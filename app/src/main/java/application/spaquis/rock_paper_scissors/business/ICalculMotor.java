package application.spaquis.rock_paper_scissors.business;

import application.spaquis.rock_paper_scissors.constantes.ActionEnum;
import application.spaquis.rock_paper_scissors.exception.BusinessException;
import application.spaquis.rock_paper_scissors.model.ResultModel;

/**
 * Interface representation le moteur de calcul
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public interface ICalculMotor {

    // -------------------------------------- Méthodes publiques

    /**
     * Méthode permettant de réaliser une partie
     *
     * @param action1 : l'action du player
     * @return : Le résultat de la partie
     */
    public ResultModel proceedPlayerGame(ActionEnum action1) throws BusinessException;

    /**
     * Méthode permettant de réaliser une partie
     *
     * @return : Le résultat de la partie
     */
    public ResultModel proceedComputerGame() throws BusinessException;
}
