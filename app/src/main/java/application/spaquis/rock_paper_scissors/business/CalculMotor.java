package application.spaquis.rock_paper_scissors.business;

import android.app.Notification;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import application.spaquis.rock_paper_scissors.constantes.ActionEnum;
import application.spaquis.rock_paper_scissors.constantes.CombinaisonEnum;
import application.spaquis.rock_paper_scissors.constantes.PlayerEnum;
import application.spaquis.rock_paper_scissors.exception.BusinessException;
import application.spaquis.rock_paper_scissors.factory.PlayerFactory;
import application.spaquis.rock_paper_scissors.model.PlayerModel;
import application.spaquis.rock_paper_scissors.model.ResultModel;

/**
 * Classe permettant de gérer le moteur de calcul
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public class CalculMotor implements ICalculMotor {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques privés
    /* Le tag de la classe */
    private static final String TAG = "CalculMotor";

    /* Nombre d'action possible */
    private static final int NB_ACTION = ActionEnum.values().length;

    /* Liste Action */
    private static final List<ActionEnum> listeAction = new ArrayList<>();

    /* Random */
    private static final Random RANDOM = new Random();

    /* Singleton */
    private static ICalculMotor instance;

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    /**
     * Constructeur
     */
    public CalculMotor() {
        for (ActionEnum action : ActionEnum.values()) {
            listeAction.add(action);
        }
    }


    // -------------------------------------- Méthodes statiques publiques

    /**
     * Méthode permettant de récupérer l'instance du Singleton
     *
     * @return : instance du Singleton
     */
    public static ICalculMotor getInstance() {
        if (instance == null) {
            instance = new CalculMotor();
        }
        return instance;
    }

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    /**
     * Méthode permettant de récupérer une action alétoire
     *
     * @return
     */
    private ActionEnum randomAction() {
        return listeAction.get(RANDOM.nextInt(NB_ACTION));
    }

    // -------------------------------------- Méthodes protected

    /**
     * Réalisation du match
     *
     * @param action1 : action 1
     * @param action2 : action 2
     * @return : le résultat
     */
    protected String proceedMatch(ActionEnum action1, ActionEnum action2, PlayerModel player1, PlayerModel player2) {
        String resultat = null;
        if (action1.equals(action2)) {
            resultat = "Equality";
        } else {
            // Duel
            CombinaisonEnum combinaison = CombinaisonEnum.getCombinaison(action1, action2);
            Log.i(TAG, "Winner Action is  : " + combinaison.getGagnant());
            if (combinaison.getGagnant().equals(action1)) {
                resultat = player1.getName() + " wins.";
            } else {
                resultat = player2.getName() + " wins.";
            }
        }
        Log.i(TAG, "The result of fight is  : " + resultat);
        return resultat;
    }

    // -------------------------------------- Méthodes publiques


    /**
     * Méthode permettant de réaliser une partie
     *
     * @param action1 : l'action du player
     * @return : Le résultat de la partie
     */
    @Override
    public ResultModel proceedPlayerGame(ActionEnum action1) throws BusinessException {
        Log.d(TAG, "Début de proceedPlayerGame");
        ResultModel retour = null;

        // Récupération de la valeur action 1 du player
        Log.i(TAG, "Action 1 : " + action1.getName());

        // Récupération de la valeur Aléatoire action 2
        ActionEnum action2 = this.randomAction();
        Log.i(TAG, "Action 2 : " + action2.getName());

        // Constitution du Résultat
        PlayerModel player1 = PlayerFactory.getInstance().getObj(PlayerEnum.PLAYER_1.getName());
        PlayerModel player2 = PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_2.getName());

        String resultat = this.proceedMatch(action1, action2, player1, player2);

        retour = new ResultModel(player1, action1, player2, action2, resultat);
        return retour;
    }

    /**
     * Méthode permettant de réaliser une partie
     *
     * @return : Le résultat de la partie
     */
    @Override
    public ResultModel proceedComputerGame() throws BusinessException {
        Log.i(TAG, "Début de proceedComputerGame");
        ResultModel retour = null;

        // Récupération de la valeur Aléatoire action 1
        ActionEnum action1 = this.randomAction();
        Log.i(TAG, "Action 1 : " + action1.getName());

        // Récupération de la valeur Aléatoire action 2
        ActionEnum action2 = this.randomAction();
        Log.i(TAG, "Action 2 : " + action2.getName());

        // Constitution du Résultat
        PlayerModel player1 = PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_1.getName());
        PlayerModel player2 = PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_2.getName());

        String resultat = this.proceedMatch(action1, action2, player1, player2);

        retour = new ResultModel(player1, action1, player2, action2, resultat);
        return retour;
    }

    // -------------------------------------- Accesseurs
}
