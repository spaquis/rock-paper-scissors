package application.spaquis.rock_paper_scissors.business;

import android.util.Log;

import application.spaquis.rock_paper_scissors.constantes.ActionEnum;
import application.spaquis.rock_paper_scissors.constantes.PlayerEnum;
import application.spaquis.rock_paper_scissors.exception.BusinessException;
import application.spaquis.rock_paper_scissors.factory.PlayerFactory;

/**
 * Business de lancement
 * <p>
 * Created by spaquis on 19/08/2017.
 */
public class Startup implements IStartup {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques privés
    /* Le tag de la classe */
    private static final String TAG = "Startup";

    /* Singleton */
    private static IStartup instance;

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    /**
     * Constructeur
     */
    public Startup() {
    }


    // -------------------------------------- Méthodes statiques publiques

    /**
     * Méthode permettant de récupérer l'instance du Singleton
     *
     * @return : instance du Singleton
     */
    public static IStartup getInstance() {
        if (instance == null) {
            instance = new Startup();
        }
        return instance;
    }

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    /* Player factory */
    private PlayerFactory playerFactory = PlayerFactory.getInstance();

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    /**
     * {@inheritDoc}
     */
    public void initApp() throws BusinessException {
        // Alimenter les computers
        Log.i(TAG, "Initialisation des computers");
        for (PlayerEnum player : PlayerEnum.values()) {
            playerFactory.add(player.getName());
        }

    }
}
