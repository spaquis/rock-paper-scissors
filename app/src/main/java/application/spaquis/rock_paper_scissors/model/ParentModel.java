package application.spaquis.rock_paper_scissors.model;

/**
 * Classe parente des models
 * <p>
 * Created by spaquis on 18/08/2017.
 */
abstract class ParentModel {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    /* Identifiant du model */
    protected Integer id;

    // -------------------------------------- Attributs publics

    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParentModel that = (ParentModel) o;

        return id.equals(that.id);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return id.hashCode();
    }

    // -------------------------------------- Accesseurs

    /**
     * Récupération de l'identifiant
     *
     * @return identifiant
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set de l'identifiant
     *
     * @param id : identifiant
     */
    public void setId(Integer id) {
        this.id = id;
    }
}
