package application.spaquis.rock_paper_scissors.model;

/**
 * Model représentant un joueur
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public class PlayerModel extends ParentModel {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    /* Nom du player */
    private String name;

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    /**
     * Constructeur
     *
     * @param id   : identifiant
     * @param name : nom
     */
    public PlayerModel(Integer id, String name) {
        this.id = id;
        this.name = name;
    }


    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "PlayerModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    // -------------------------------------- Accesseurs

    /**
     * Récupération du nom
     *
     * @return : nom
     */
    public String getName() {
        return name;
    }

    /**
     * Set du nom
     *
     * @param name : nom
     */
    public void setName(String name) {
        this.name = name;
    }
}
