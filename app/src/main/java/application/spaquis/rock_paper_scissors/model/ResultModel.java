package application.spaquis.rock_paper_scissors.model;

import application.spaquis.rock_paper_scissors.constantes.ActionEnum;

/**
 * Objet représentant le résultat d'une partie
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public class ResultModel extends ParentModel {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    /* Joueur 1 */
    private PlayerModel playerOne;

    /* Action 1 */
    private ActionEnum actionPlayerOne;

    /* Joueur 2 */
    private PlayerModel playerTwo;

    /* Action 2 */
    private ActionEnum actionPlayerTwo;

    /* Résultat */
    private String resultat;

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    /**
     * Constructeur
     *
     * @param playerOne
     * @param actionPlayerOne
     * @param playerTwo
     * @param actionPlayerTwo
     * @param resultat
     */
    public ResultModel(PlayerModel playerOne, ActionEnum actionPlayerOne, PlayerModel playerTwo, ActionEnum actionPlayerTwo, String resultat) {
        this.playerOne = playerOne;
        this.actionPlayerOne = actionPlayerOne;
        this.playerTwo = playerTwo;
        this.actionPlayerTwo = actionPlayerTwo;
        this.resultat = resultat;
    }


    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    @Override
    public String toString() {
        return "ResultModel{" +
                "playerOne=" + playerOne +
                ", actionPlayerOne=" + actionPlayerOne +
                ", playerTwo=" + playerTwo +
                ", actionPlayerTwo=" + actionPlayerTwo +
                ", resultat='" + resultat + '\'' +
                '}';
    }

    // -------------------------------------- Accesseurs

    /**
     * Récupération du joueur 1
     *
     * @return : joueur 1
     */
    public PlayerModel getPlayerOne() {
        return playerOne;
    }

    /**
     * Set du joueur 1
     *
     * @param playerOne : joueur 1
     */
    public void setPlayerOne(PlayerModel playerOne) {
        this.playerOne = playerOne;
    }

    /**
     * Récupération de l'action du joueur 1
     *
     * @return : action du joueur 1
     */
    public ActionEnum getActionPlayerOne() {
        return actionPlayerOne;
    }

    /**
     * Set de l'action du joueur 1
     *
     * @param actionPlayerOne : action du joueur 1
     */
    public void setActionPlayerOne(ActionEnum actionPlayerOne) {
        this.actionPlayerOne = actionPlayerOne;
    }

    /**
     * Récupération du joueur 2
     *
     * @return : joueur 2
     */
    public PlayerModel getPlayerTwo() {
        return playerTwo;
    }

    /**
     * Set du joueur 2
     *
     * @param playerTwo : joueur 2
     */
    public void setPlayerTwo(PlayerModel playerTwo) {
        this.playerTwo = playerTwo;
    }

    /**
     * Récupération de l'action du joueur 2
     *
     * @return : action du joueur 2
     */
    public ActionEnum getActionPlayerTwo() {
        return actionPlayerTwo;
    }

    /**
     * Set de l'action du joueur 2
     *
     * @param actionPlayerTwo : action du joueur 2
     */
    public void setActionPlayerTwo(ActionEnum actionPlayerTwo) {
        this.actionPlayerTwo = actionPlayerTwo;
    }

    /**
     * Récupération du résultat
     *
     * @return : le résultat
     */
    public String getResutat() {
        return resultat;
    }

    /**
     * Set du résultat
     *
     * @param resultat : le résultat
     */
    public void setResultat(String resultat) {
        this.resultat = resultat;
    }
}
