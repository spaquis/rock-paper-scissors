package application.spaquis.rock_paper_scissors;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import application.spaquis.rock_paper_scissors.business.Startup;
import application.spaquis.rock_paper_scissors.constantes.ModeEnum;
import application.spaquis.rock_paper_scissors.exception.BusinessException;

/**
 * Activity de la page d'accueil
 * <p>
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public class HomeActivity extends AppCompatActivity {


    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques privés

    /* Le tag de la classe */
    private static final String TAG = "HomeActivity";
    // -------------------------------------- Attributs statiques publics

    /* Token representant un mode */
    public static final String MODE_TOKEN = "MODE";

    // -------------------------------------- Attributs privés

    /* le layout des buttons */
    private LinearLayout layoutButton;

    /* Les différents mode possibles */
    private Button[] btnMode;

    /* Listener sur le click d'un mode de jeu */
    private final View.OnClickListener btnClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.i(TAG, "Click sur le mode " + v.getTag());
            Object tag = v.getTag();
            // On redirige sur le bon mode
            goToGame((Integer) tag);
        }
    };

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    /**
     * Méthode permettant d'initialiser l'app
     */
    private void initialiseApp() {
        Log.i(TAG, "Inialisation de l'application");
        try {
            Startup.getInstance().initApp();
        } catch (BusinessException b) {
            Toast.makeText(getApplicationContext(), "Une erreur est survenue", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Méthode permettant de créer un button mode
     *
     * @param mode : le mode
     * @return : le button
     */
    private Button createButtonMode(ModeEnum mode) {
        Button button = new Button(this);
        button.setTag(mode.getId());
        button.setOnClickListener(btnClicked);
        button.setText(mode.getName());
        return button;
    }

    /**
     * Méthode permettant de se rediriger vers la page Game
     *
     * @param modeId : l'identifiant du mode
     */
    private void goToGame(Integer modeId) {
        Log.d(TAG, "Redirection vers l'activity Game");
        Intent intent = new Intent(this, GameActivity.class);

        intent.putExtra(MODE_TOKEN, modeId);
        startActivity(intent);
    }

    // -------------------------------------- Méthodes protected

    /**
     * Méthode de création de l'activity
     *
     * @param savedInstanceState : le state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Création de l'activity");
        super.onCreate(savedInstanceState);
        this.initialiseApp();
        setContentView(R.layout.activity_home);

        // Récupération des composants
        layoutButton = (LinearLayout) findViewById(R.id.buttons_layout);

        Log.d(TAG, "Création des buttons mode");
        btnMode = new Button[ModeEnum.values().length];
        for (ModeEnum modeEnum : ModeEnum.values()) {
            Button button = this.createButtonMode(modeEnum);
            layoutButton.addView(button);
        }

        /* mTextMessage = (TextView) findViewById(R.id.message);
         BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener); */

    }

    // -------------------------------------- Méthodes publiques

    // -------------------------------------- Accesseurs


}
