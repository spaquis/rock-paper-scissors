package application.spaquis.rock_paper_scissors.business;

import org.junit.Before;
import org.junit.Test;

import application.spaquis.rock_paper_scissors.constantes.ActionEnum;
import application.spaquis.rock_paper_scissors.constantes.PlayerEnum;
import application.spaquis.rock_paper_scissors.exception.BusinessException;
import application.spaquis.rock_paper_scissors.factory.PlayerFactory;
import application.spaquis.rock_paper_scissors.model.ResultModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Classe de test de calcul motor
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public class CalculMotorTest {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques privés

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    /* Business à tester */
    private ICalculMotor business = CalculMotor.getInstance();

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    @Before
    public void init() throws BusinessException {
        PlayerFactory.getInstance().add(PlayerEnum.PLAYER_1.getName());
        PlayerFactory.getInstance().add(PlayerEnum.COMPUTER_1.getName());
        PlayerFactory.getInstance().add(PlayerEnum.COMPUTER_2.getName());
    }


    // COMMENTER CAR UTILISATION INTERFACE
    /**
     * Test de papier / ciseau
     */
    /* @Test
    public void test_paper_scissor() throws BusinessException {
        // Prepare

        // Act
        String resultat = business.proceedMatch(ActionEnum.PAPER, ActionEnum.SCISSORS, PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_1.getName()), PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_2.getName()));

        // Assert
        assertEquals(PlayerEnum.COMPUTER_2.getName() + " a gagné.", resultat);

        // Act
        String resultat2 = business.proceedMatch(ActionEnum.SCISSORS, ActionEnum.PAPER, PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_1.getName()), PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_2.getName()));

        // Assert
        assertEquals(PlayerEnum.COMPUTER_1.getName() + " a gagné.", resultat2);
    } */

    // COMMENTER CAR UTILISATION INTERFACE
    /**
     * Test papier / pierre
     */
    /* @Test
    public void test_paper_rock() throws BusinessException {
        // Prepare

        // Act
        String resultat = business.proceedMatch(ActionEnum.PAPER, ActionEnum.ROCK, PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_1.getName()), PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_2.getName()));

        // Assert
        assertEquals(PlayerEnum.COMPUTER_1.getName() + " a gagné.", resultat);

        // Act
        String resultat2 = business.proceedMatch(ActionEnum.ROCK, ActionEnum.PAPER, PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_1.getName()), PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_2.getName()));

        // Assert
        assertEquals(PlayerEnum.COMPUTER_2.getName() + " a gagné.", resultat2);
    } */

    // COMMENTER CAR UTILISATION INTERFACE
    /**
     * Test ciseau / pierre
     */
    /* @Test
    public void test_scissors_rock() throws BusinessException {
        // Prepare

        // Act
        String resultat = business.proceedMatch(ActionEnum.SCISSORS, ActionEnum.ROCK, PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_1.getName()), PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_2.getName()));

        // Assert
        assertEquals(PlayerEnum.COMPUTER_2.getName() + " a gagné.", resultat);

        // Act
        String resultat2 = business.proceedMatch(ActionEnum.ROCK, ActionEnum.SCISSORS, PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_1.getName()), PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_2.getName()));

        // Assert
        assertEquals(PlayerEnum.COMPUTER_1.getName() + " a gagné.", resultat2);
    } */

    // COMMENTER CAR UTILISATION INTERFACE
    /**
     * Test de egalité
     */
    /* @Test
    public void test_equality() throws BusinessException {
        // Prepare

        // Act
        String resultat = business.proceedMatch(ActionEnum.SCISSORS, ActionEnum.SCISSORS, PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_1.getName()), PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_2.getName()));

        // Assert
        assertEquals("Egalité", resultat);

        // Act
        String resultat2 = business.proceedMatch(ActionEnum.ROCK, ActionEnum.ROCK, PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_1.getName()), PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_2.getName()));

        // Assert
        assertEquals("Egalité", resultat2);

        // Act
        String resultat3 = business.proceedMatch(ActionEnum.PAPER, ActionEnum.PAPER, PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_1.getName()), PlayerFactory.getInstance().getObj(PlayerEnum.COMPUTER_2.getName()));

        // Assert
        assertEquals("Egalité", resultat3);
    } */

    /**
     *
     */
    @Test
    public void test_proceedPlayerVsComputer() throws BusinessException {
        // Prepare

        // Act
        ResultModel resultat1 = business.proceedPlayerGame(ActionEnum.PAPER);

        ResultModel resultat2 = business.proceedPlayerGame(ActionEnum.ROCK);

        ResultModel resultat3 = business.proceedPlayerGame(ActionEnum.PAPER);

        // Assert
        // Assert compliqué car valeur aléatoire
        assertNotNull(resultat1);
        assertNotNull(resultat2);
        assertNotNull(resultat3);
    }

    /**
     *
     */
    @Test
    public void test_proceedComputerVsComputer() throws BusinessException {
        // Prepare

        // Act
        ResultModel resultat1 = business.proceedComputerGame();

        ResultModel resultat2 = business.proceedComputerGame();

        ResultModel resultat3 = business.proceedComputerGame();

        // Assert
        // Assert compliqué car valeur aléatoire
        assertNotNull(resultat1);
        assertNotNull(resultat2);
        assertNotNull(resultat3);
    }


    // -------------------------------------- Accesseurs
}
