package application.spaquis.rock_paper_scissors.business;

import org.junit.Before;
import org.junit.Test;

import application.spaquis.rock_paper_scissors.exception.BusinessException;
import application.spaquis.rock_paper_scissors.factory.PlayerFactory;

import static org.junit.Assert.assertEquals;

/**
 * Classe de test de Startup
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public class StartupTest {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques privés

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    /* Business à tester */
    private IStartup business = Startup.getInstance();

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    @Before
    public void init() {
        // On reset la map
        PlayerFactory.getInstance().clearMap();
    }

    /**
     * Test de l'ajout d'une action
     */
    @Test
    public void test_initApp() throws BusinessException {
        // Prepare

        // Act
        business.initApp();

        // Assert
        assertEquals(3, PlayerFactory.getInstance().getTailleMap());
    }

    // -------------------------------------- Accesseurs
}
