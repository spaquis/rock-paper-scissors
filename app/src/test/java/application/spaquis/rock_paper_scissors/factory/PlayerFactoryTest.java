package application.spaquis.rock_paper_scissors.factory;

import org.junit.Before;
import org.junit.Test;

import application.spaquis.rock_paper_scissors.exception.BusinessException;
import application.spaquis.rock_paper_scissors.model.PlayerModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Classe de test de PlayerFactory
 * <p>
 * Created by spaquis on 18/08/2017.
 */
public class PlayerFactoryTest {

    // -------------------------------------- Inner classes

    // -------------------------------------- Attributs statiques privés

    // -------------------------------------- Attributs statiques publics

    // -------------------------------------- Attributs privés

    // -------------------------------------- Attributs publics

    // -------------------------------------- Constructeur

    // -------------------------------------- Méthodes statiques publiques

    // -------------------------------------- Méthodes statiques privées

    // -------------------------------------- Méthodes privées

    /* Player Factory **/
    private PlayerFactory playerFactory = PlayerFactory.getInstance();

    // -------------------------------------- Méthodes protected

    // -------------------------------------- Méthodes publiques

    /**
     * Test de l'ajout d'un joueur
     */
    @Test
    public void test_Add_Player() throws BusinessException {
        // Prepare
        String name = "jean";

        // Act
        PlayerModel player = playerFactory.add(name);

        // Assert
        assertNotNull(player);
        assertNotNull(player.getId());
        assertEquals(name, player.getName());
    }

    /**
     * Test de l'ajout d'un joueur existant déjà
     */
    @Test
    public void test_Player_Already_Exist() throws BusinessException {
        // Prepare
        String name = "seb";

        // Act
        PlayerModel player = playerFactory.add(name);
        PlayerModel player2 = playerFactory.add(name);

        // Assert
        assertEquals(player, player2);
    }

    // -------------------------------------- Accesseurs
}
