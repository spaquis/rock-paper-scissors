# Rock, Paper, Scissors

Application Android Permettant de jouer au pierre / feuille / ciseaux selon 2 mode (Player VS Computer et Computer VS Computer).

# Getting started

* Cloner le repository 
* Installer Android Studio
* Importer le projet dans Android Studio
* Lancer l'application

# Tests

Tests JUnit dans le package test.

# Nouvelles Actions

L'application est conçue de façon à pouvoir ajouter instantanément des nouvelles actions. 

   * 1-  Se rendre dans la classe : package application.spaquis.rock_paper_scissors.constantes.ActionEnum et ajouter votre action :

```
#!

ROCK(1, "Rock"), PAPER(2, "Paper"), SCISSORS(3, "Scissors"), SPOCK(4, "Spock");

```

  * 2- Se rendre dans la classe : package application.spaquis.rock_paper_scissors.constantes.CombinaisonEnum et ajouter les combinaisons possibles avec la nouvelle action


```
#!
    PAPER_SPOCK(ActionEnum.PAPER, ActionEnum.SPOCK, ActionEnum.SPOCK),
    SPOCK_SCISSORS(ActionEnum.SPOCK, ActionEnum.SCISSORS, ActionEnum.SCISSORS),
    SPOCK_ROCK(ActionEnum.ROCK, ActionEnum.SPOCK, ActionEnum.ROCK);

```

C'est tout, recompilez votre application et Enjoy !